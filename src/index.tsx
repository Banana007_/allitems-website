import React from 'react';
import ReactDOM from 'react-dom/client';
import { WebServerInstance } from './websocket/websocket';
import { gameRegistry } from './pages/game';
import "./index.css";
import { AllItemsGame } from './pages/layouts/all-items/allitems-connector';

const Loading = React.lazy(() => import('./pages/Loading'));
const AllItems = React.lazy(() => import('./pages/layouts/all-items/allitems'));
let test = "Waiting for Server...";

const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement);

gameRegistry.registerGame(new AllItemsGame());



if (!WebServerInstance(root).connected) {
  console.log("connect...");
  WebServerInstance(root).connect("localhost:5001", () => {
    test = "Waiting for Minigame to start...";
   root.render(
      <Loading user={test} />
   );

  }, () => {
    test = "Could not connect to Server!";
    root.render(
      <Loading user={test} />
    )
  });
}


