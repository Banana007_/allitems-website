import { gameRegistry } from "../pages/game";
import ReactDOM from 'react-dom/client';
import sound from './party.mp3';

class WebServerUtil {
    connected: boolean = false;
    private root;

    constructor(root: ReactDOM.Root) {
        this.root = root;
    }

    connect(host: string, success: () => void, failed: () => void) {
        try {
            let socket = new WebSocket("ws://" + host);

            socket.onerror = (event) => {
                failed();
            };
            
        socket.addEventListener("open", event => {
            this.connected = true;
            success();
            socket.send("Connection established");
        });


        // Listen for messages
        socket.addEventListener("message", event => {
            console.log(event.data);

            if (event.data === "refresh-view") {
                window.location.reload();
                console.log("refresh!");
                return;
            }

            if (event.data === "party-start") {
                let audio = new Audio(sound);
                audio.play();
                return;
            }

            let body = JSON.parse(event.data);

            let a = body["challenge-status"] !== undefined;
            

            if (body["challenge-status"] !== undefined && gameRegistry.hasTargetGame()) {
                console.log("AA");
                let status = body["challenge-status"];

                if (status === "PAUSED") {
                    gameRegistry.getTargetGame()?.onBreak(this.root);
                } else if (status === "RESUME") {
                    gameRegistry.getTargetGame()?.onResume(this.root);
                } else {
                    console.warn("[WBS] Unkown challenge status " + status);
                }
                return;
            }


            if (!gameRegistry.hasTargetGame()) {
                gameRegistry.getGames().forEach(e => {

                    console.log(e.getIdentifier());

                    if (e.getIdentifier() === body["challenge-type"]) {
                        console.log("Target!");
                        gameRegistry.setTargetGame(e);
                        e.onTargetWebSocketMessage(this.root, body);
                        e.onStart(this.root);
                        this.root.render(e.getComponentToRender());
                    }
                });
            } else {
                console.log("Message!");
                gameRegistry.getTargetGame()?.onTargetWebSocketMessage(this.root, body);
            }

            if (!gameRegistry.hasTargetGame()) {
                console.warn("[WBS] Can't find game or json key 'challenge-type' for identifier!");
            }

        });
        } catch(any) {

            failed();
        }
    }
}

export interface WebServerHook {
    getMessage(message: string): () => (string);
}


export const WebServerInstance = (targetRoot: ReactDOM.Root) => {
    return new WebServerUtil(targetRoot);
  };
