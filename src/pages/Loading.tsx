import './Loading';
import '../index';
import { Component, ReactNode } from 'react';

class Loading extends Component<{user: string}> {

  constructor(props: any) {
    super(props);
  }

  render(): ReactNode {
    return (
      <div className='basic-container'>
      <p>{this.props.user}</p>
    </div>
    );
  }

  user() {
  }

}

export default Loading;
