import ReactDOM from 'react-dom/client';

export interface Game {
    getComponentToRender(): any;
    getIdentifier(): string;
    onStart(root: ReactDOM.Root): void;
    onBreak(root: ReactDOM.Root): void;
    onTargetWebSocketMessage(root: ReactDOM.Root, message: any): void;
    onBreak(root: ReactDOM.Root): void;
    onResume(root: ReactDOM.Root): void;
}

class GameRegistry {
    private games: Game[] = [];
    private targetGame?: Game = undefined;

    registerGame(game: Game) {
        this.games.push(game);
        console.log("Register Game!");
    }

    getGames(): Game[] {
        return this.games;
    }

    setTargetGame(game: Game) {
        this.targetGame = game;
    }

    hasTargetGame(): boolean {
        return this.targetGame != undefined;
    }

    getTargetGame(): Game | undefined {
        return this.targetGame;
    }
}

export const gameRegistry = new GameRegistry();
export default Game;