import { Component, ReactNode } from 'react';
import moment from 'moment';
import { Properties } from './allitems-connector';
import './allitems.css';

class AllItems extends Component {
  render(): ReactNode {
    return (
        <div className='basic-container container'>
            <div id='amount'>
                <p style={{fontWeight: "bold"}}>{Properties.currentItem}</p>
                <p style={{fontSize: "2.0vw"}}>von</p>
                <p style={{fontSize: "2.5vw"}}>{Properties.maxItem}</p>
            </div>
            <div id='block'><img src={Properties.currentItemImage}/></div>
            <div id='description'>
                <p>{Properties.currentItemName}</p>
                <p id='timer' style={{color: Properties.color}}>{fancyTimer(Properties.timer)}</p>
            </div>
        </div>
      );
  }
}



function fancyTimer(time: number): string  {
    var h = Math.floor(time % (3600*24) / 3600);
    var m = Math.floor(time % 3600 / 60);
    var s = Math.floor(time % 60);
    return moment().hours(h).minutes(m).seconds(s).format("HH:mm:ss");
}

export default AllItems;