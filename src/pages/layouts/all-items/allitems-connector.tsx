import Game from "../../game";
import AllItems from "./allitems";
import ReactDOM from 'react-dom/client';

export const Properties = {
    currentItem: 0,
    maxItem: 0,
    currentItemImage: "https://cdn.apexminecrafthosting.com/img/uploads/2022/03/28151238/elytra.png",
    currentItemName: "undefined",
    timer:  0,
    color: "yellow"
};


export class AllItemsGame implements Game {
    private timerId: any;


    getComponentToRender() {
        return <AllItems/>;
    }

    getIdentifier(): string {
        return "all-items";
    }

    onStart(root: ReactDOM.Root): void {
        this.timerId = setInterval(() => {
            Properties.timer++;
            root.render(this.getComponentToRender());
        }, 1000);
        console.log(Properties.timer);
        console.log("BREAK");
    }

    onResume(root: ReactDOM.Root): void {
        this.onStart(root);

        let element = document.getElementById("timer_paused");

        if (element) {
            element.id = "timer";
        }
        Properties.color = "yellow";
        root.render(this.getComponentToRender());
    }

    onBreak(root: ReactDOM.Root): void {
        clearInterval(this.timerId);
        Properties.color = "grey";
        console.log("BREAK");
        root.render(this.getComponentToRender());

    }

    onTargetWebSocketMessage(root: ReactDOM.Root, message: any): void {
        let challengeProperties = message["challenge-properties"];
        
        let itemCurrent = challengeProperties["item-current"];
        let itemIndex = challengeProperties["item-index"];
        let itemMax = challengeProperties["item-max"];
        let imageUrl = challengeProperties["item-url"];
        let itemTimerStart = challengeProperties["item-timer-start"];

        Properties.currentItemName = itemCurrent;
        Properties.currentItem = itemIndex;
        Properties.currentItemImage = imageUrl;
        Properties.maxItem = itemMax;
        Properties.timer = itemTimerStart;
    }
}